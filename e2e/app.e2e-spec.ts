import { EverestBaseUiPage } from './app.po';

describe('everest-base-ui App', () => {
  let page: EverestBaseUiPage;

  beforeEach(() => {
    page = new EverestBaseUiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
