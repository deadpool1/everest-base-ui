import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Location }                 from '@angular/common';
import { Observable }        from 'rxjs/Observable';
import { Subject }           from 'rxjs/Subject';

// Observable class extensions
import 'rxjs/add/observable/of';

// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

import { CustomersService } from './customers.service';
import { Customer } from './customer';

@Component({
  selector: 'customer-detail',
  templateUrl: './customer-detail.component.html',
  providers: [CustomersService]
})
export class CustomerDetailComponent implements OnInit {
  customer: Customer;
  attr: Object;
  constructor(
    private customersService: CustomersService,
    private route: ActivatedRoute,
    private location: Location,    
    private router: Router) {}

  ngOnInit(): void {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.customersService.getCustomer(+params.get('id')))
      .subscribe(customer => {this.customer = customer});
  }
}