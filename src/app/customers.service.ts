import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {Headers} from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Customer }           from './customer';

@Injectable()
export class CustomersService {

  constructor(private http: Http) {} 

  getBaseUrl(): string {
    const url = `http://alpha.pramatadev.com:3002/api/v1`;
    return url;
  }

  getHeaders(): Headers {
    let username: string = 'everest_user';
    let password: string = 'everest_poc@321';
    let headers: Headers = new Headers();
    headers.append("Authorization", "Basic " + btoa(username + ":" + password)); 
    return headers;
  }

  getCustomers(): Promise<Customer[]> {
    const url = `${this.getBaseUrl()}/customers`;
    return this.http.get(url,{headers: this.getHeaders()})
      .toPromise()
      .then(response => response.json().data as Customer[])
      .catch(this.handleError);      
  }    

  getCustomer(id: number): Promise<Customer> {
    const url = `${this.getBaseUrl()}/customers/${id}`;
    return this.http.get(url,{headers: this.getHeaders()})
      .toPromise()
      .then(response => response.json().data as Customer)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }  
}

