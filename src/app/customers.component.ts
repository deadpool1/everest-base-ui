import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';

import { Observable }        from 'rxjs/Observable';
import { Subject }           from 'rxjs/Subject';

// Observable class extensions
import 'rxjs/add/observable/of';

// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { CustomersService } from './customers.service';
import { Customer } from './customer';

@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  providers: [CustomersService]
})
export class CustomersComponent implements OnInit {
  customers: Customer[];
  constructor(
    private customersService: CustomersService,
    private router: Router) {}

  getCustomers(): void {
    this.customersService
      .getCustomers()
      .then(customers => this.customers = customers);
  } 

  ngOnInit(): void {
    this.getCustomers();  
  }
}