import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent }      from './home.component';
import { CustomersComponent }  from './customers.component';
import { CustomerDetailComponent }  from './customer-detail.component';
import { AnalyticsComponent }  from './analytics.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home',  component: HomeComponent },
  { path: 'customers',  component: CustomersComponent },
  { path: 'customers/:id',  component: CustomerDetailComponent },
  { path: 'analytics',  component: AnalyticsComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}