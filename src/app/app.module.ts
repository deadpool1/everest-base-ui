import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { HomeComponent }   from './home.component';
import { AppComponent }         from './app.component';
import { CustomersComponent }   from './customers.component';
import { AnalyticsComponent }   from './analytics.component';
import { CustomerDetailComponent }   from './customer-detail.component';
import { CustomersService }          from './customers.service';

import { AppRoutingModule } from './app-routing.module';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    CustomersComponent,
    HomeComponent,
    AnalyticsComponent,
    CustomerDetailComponent    
  ],
  providers: [ CustomersService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }